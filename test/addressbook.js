var expect = require("chai").expect;
var addressbook = require("../app/addressbook.js");

describe("Testing module functions:", function() {

	describe("How many women are in the address book?", function() {
		it("Answer the question 1:", function() {
			var answertoQuestion1 = addressbook.getAnswer(1);
			expect(answertoQuestion1).to.equal(2);
		});
	});

	describe("Who is the oldest person in the address book?", function() {
		it("Answer the question 2:", function() {
			var answertoQuestion2 = addressbook.getAnswer(2);
			expect(answertoQuestion2).to.equal("Wes Jackson");
		});
	});

	describe("How many days older is Bill than Paul?", function() {
		it("Answer the question 3:", function() {
			var answertoQuestion3 = addressbook.getAnswer(3);
			expect(answertoQuestion3).to.equal(2862);
		});
	});

});