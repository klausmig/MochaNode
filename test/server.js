var expect = require("chai").expect;
var request = require("request");
global.url = "http://localhost:3000/questions"

describe("Testing server functions:", function() {

	describe("How many women are in the address book?", function() {

		var url = global.url + "/1";

		it("returns status 200", function(done) {
			request(url, function(error, response, body) {
				expect(response.statusCode).to.equal(200);
				done();
			});
		});

		it("Answer the question 1:", function(done) {
			request(url, function(error, response, body) {
				expect(body).to.equal('1.2<br>');
				done();
			});
		});

	});

	describe("Who is the oldest person in the address book?", function() {

		var url = global.url + "/2";

		it("returns status 200", function(done) {
			request(url, function(error, response, body) {
				expect(response.statusCode).to.equal(200);
				done();
			});
		});

		it("Answer the question 2:", function(done) {
			request(url, function(error, response, body) {
				expect(body).to.equal('2.Wes Jackson<br>');
				done();
			});
		});

	});

	describe("How many days older is Bill than Paul?", function() {

		var url = global.url + "/3";

		it("returns status 200", function(done) {
			request(url, function(error, response, body) {
				expect(response.statusCode).to.equal(200);
				done();
			});
		});

		it("Answer the question 2:", function(done) {
			request(url, function(error, response, body) {
				expect(body).to.equal('3.2862<br>');
				done();
			});
		});

	});

	describe("Answer all questions", function() {

		var url = global.url;

		it("returns status 200", function(done) {
			request(url, function(error, response, body) {
				expect(response.statusCode).to.equal(200);
				done();
			});
		});

		it("Answer all:", function(done) {
			request(url, function(error, response, body) {
				expect(body).to.equal('1.2<br>2.Wes Jackson<br>3.2862<br>');
				done();
			});
		});

	});



});