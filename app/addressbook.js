var fs = require('fs');
var lineArray;
global.addressBook = new Array();

var contents = fs.readFileSync('addressbook.txt').toString();

lineArray = contents.split("\n");

for (i in lineArray) {
	var personArray = lineArray[i].toString().split(",");
	global.addressBook.push(personArray);
}

exports.getAnswer = function(questionNumber) {

	switch (questionNumber) {
		case 1: //How many women are in the address book?
			var femaleCounter = 0;

			for (i in addressBook) {

				if (addressBook[i][1].trim() == "Female") femaleCounter++;
			}

			return femaleCounter;
			break;
		case 2: //Who is the oldest person in the address book?
			var olderAge = new Date();
			var index = 0;

			for (i in addressBook) {

				var d1 = addressBook[i][2].split("/");
				var birthday = new Date(d1[2], d1[1], d1[0]);

				if (birthday < olderAge) {
					olderAge = birthday;
					index = i;
				}
			}
			//console.log("sol:" + addressBook[index][0].trim());
			return addressBook[index][0].trim();
			break;
		case 3: //How many days older is Bill than Paul?
			var datePaul;
			var dateBill;

			for (i in addressBook) {

				if (addressBook[i][0].indexOf("Bill") !== -1) {

					var d1 = addressBook[i][2].split("/");
					dateBill = new Date(d1[2], d1[1], d1[0]);

				}

				if (addressBook[i][0].indexOf("Paul") !== -1) {

					var d1 = addressBook[i][2].split("/");
					datePaul = new Date(d1[2], d1[1], d1[0]);

				}

			}

			return days_between(dateBill, datePaul);
			break;
	}

};


function days_between(date1, date2) {

	// The number of milliseconds in one day
	var ONE_DAY = 1000 * 60 * 60 * 24

	// Convert both dates to milliseconds
	var date1_ms = date1.getTime()
	var date2_ms = date2.getTime()

	// Calculate the difference in milliseconds
	var difference_ms = Math.abs(date1_ms - date2_ms)

	// Convert back to days and return
	return Math.round(difference_ms / ONE_DAY)

}