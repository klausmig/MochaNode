var express = require("express");
var app = express();

var addressbook = require("./addressbook.js");

app.get("/questions/:number", function(req, res) {
	var questionNumber = parseInt(req.params.number);
	var answer = addressbook.getAnswer(questionNumber);
	res.send(questionNumber + '.' + answer + "<br>");
});


app.get("/questions", function(req, res) {
	var totalAnswer = "";

	for (var i = 1; i < 4; i++) {
		var answer = addressbook.getAnswer(i);
		totalAnswer += (i + '.' + answer + "<br>");
	}

	res.send(totalAnswer);

});

app.listen(3000);